const db = require("./db_config");

db.connect(function(err) {
    if (err) throw err;
    
    const sql = "CREATE DATABASE MQTT_JS"; // Database name 
    db.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Database created");
    });
});
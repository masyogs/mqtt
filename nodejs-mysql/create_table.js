const db = require("./db_config");

db.connect(function(err){
	if (err) throw err;
	const sql = `CREATE TABLE data_sensor 
	(
		id int NOT NULL AUTO_INCREMENT,
		temperature varchar(255),
		time timestamp default current_timestamp,
		PRIMARY KEY (id)
	)`; // cretae table name
	db.query(sql, function(err, result){
		if (err) throw err;
		console.log("Table created");
	});
});